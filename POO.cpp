#include "pch.h"

#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
    int nr_persoane;
    cout << "Numar persoane: ";
    cin >> nr_persoane;

    vector<pair<string, int>> memorie;

    for (int i = 0; i < nr_persoane; i++) {
        string nume;
        int varsta;

        cout << "Nume: " ;
        cin >> nume;

        cout << "Varsta: " ;
        cin >> varsta;

        memorie.push_back(make_pair(nume, varsta));
    }


    cout << "Afisare rezultate:" <<endl;
    for (int i = 0; i < nr_persoane; i++) {
        cout  << memorie[i].first << " are " << memorie[i].second << " ani." << endl;
    }

    return 0;
}
