#include "pch.h"
#include <iostream>

using namespace System;
using namespace std;

int main()
{
	try
	{
		int age = 18;
		if (age >= 19) {
			cout << "Salut!";
		}
		else {
			throw (age);
		}
	}
	catch (int myAger)
	{
		cout << "Esti prea mic/a pentru a asta.\n";
		cout << "Varsta ta e: " << myAger;
	}
    return 0;
}

