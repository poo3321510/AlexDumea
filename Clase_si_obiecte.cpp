#include "pch.h"
#include <iostream>
#include <string>


using namespace System;
using namespace std;


class materiale {
    public:
         
        double nr_bucati;
        double pret;

        double afisare_valoare() {
            return nr_bucati*pret;
        
        }
};

int main()
{

    materiale lemne;

    lemne.nr_bucati = 250;
    lemne.pret = 43;

    cout << " Valoarea totala a lemnului este = " << lemne.afisare_valoare() << " lei" << endl;

    return 0;
}
